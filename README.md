[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

RSS-Fusion-Démo (version 0.7.0b)
----------
**Exemple d'utilisation de [RSS-Fusion]** à travers un outil permettant de fusionner plusieurs flux RSS/ATOM afin de n'en former qu'un seul, lui même filtré par un ensemble de mots clés conditionnant l'affichage ou non des items.

En vrac : 
 * Fusion de flux RSS multiples
 * Sans base de données
 * Script auto hébergeable facilement
 * Gestion des flux RSS/ATOM
 * Génération flux global
 * Surcharge de personnalisation
 * Système de mise en cache des flux
 * Données triées par date/heure décroissante
 * Possibilité de spécifier des mots clés (pour mettre en avant ou mettre de côté des items)
 * Suppression automatique des fichiers de configuration non consultés depuis plus de 30 jours
 * Gestion de token
 * Support multilingue


Démo en ligne
----------
Une **[démo en ligne]** est parfois disponible, selon la sucharge du serveur en mousse qui l'héberge. 


Prérequis et installation
---------
 * Récupérer les sources
```
$ git clone https://git.framasoft.org/Erase/RSS-Fusion-Demo.git
```
ou [télécharger l'archive zip]
 * Vérifier/assigner les droits en écriture au sein du répertoire `/c`
 * Voir le fichier `README.md` de [RSS-Fusion] le paramétrage du module, contenu dans le répertoire `/vendor/RSS-Fusion`


Ajouter une langue
-------
 Suivant le format des fichiers déjà présents dans le répertoire `/lang`, ajoutez un fichier `[lang].json` (en remplaçant `[lang]` par celle de votre choix) et effectuez la traduction. Ce fichier se présente sous un format JSON suivant le principe suivant : `[cle] : [traduction]`. Ainsi, les clés ne doivent pas être changées, seule la partie traduction est à modifier. 

 Enfin, ajouter la ligne suivante en fin du fichier `/vendor/RSS-Fusion/system/config/localconfig.php` : 

 ```php
 	$GLOBALS['TL_CONFIG']['language'] = "[lang]"; //~ Où "[lang]" correspond au nom du fichier précédement crée
 ```



Licence
-------
 En dehors des différentes licences spécifiques aux outils utilisés, le reste du code est distribué sous licence [Creative Commons BY-NC-SA 4.0]


Auteur
------
 * [Un simple développeur paysagiste] :) avec un peu de temps libre et aucune prétention - contact_at_green-effect.fr

Et les copains

 * Sur une idée de [Liandri]
 * Avec le soutien et des morceaux de code (tel que [goofi]) de [Bronco]



[//]: # 
   [RSS-Fusion]: <https://framagit.org/Erase/RSS-Fusion>
   [télécharger l'archive zip]: <https://framagit.org/Erase/RSS-Fusion-Demo/repository/archive.zip>
   [Creative Commons BY-NC-SA 4.0]: <http://creativecommons.org/licenses/by-nc-sa/4.0/>
   [Un simple développeur paysagiste]: <http://www.green-effect.fr>
   [démo en ligne]: <https://rss-fusion.green-effect.fr/>
   [Liandri]: <https://libox.fr/>
   [goofi]: <https://github.com/broncowdd/goofi>
   [Bronco]: <http://warriordudimanche.net/>